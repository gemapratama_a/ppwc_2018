from django.db import models
from django.utils import timezone
from datetime import datetime, date
import datetime

class Schedule(models.Model):
    hari     = models.CharField(max_length = 200, default='1')
    tanggal  = models.DateField(default = datetime.date.today)
    jam      = models.TimeField(default = datetime.time(00, 00))
    kegiatan = models.CharField(max_length = 200)
    tempat   = models.CharField(max_length = 200)
    kategori = models.CharField(max_length = 200)

    def __str__(self):
        return self.kegiatan    # Agar nama kegiatan muncul saat database diakses
