from django.shortcuts import render, redirect
from Lab3PPW.forms import Form
from Lab3PPW.models import Schedule
from django.http import HttpResponseRedirect
from django.http import HttpResponse
# Inspirasi dari https://www.youtube.com/watch?v=b8RpVs7bSgo
# Inspirasi dari https://www.youtube.com/watch?v=VxOsCKMStuw

def get(request):
    form = Form()
    user_data = Schedule.objects.all()
    args = {'form' : form, 'user_data' : user_data}
    return render(request, 'Lab3PPW/displayjadwal.html', args)

def delete_all(request):
    user_data_empty = Schedule.objects.all().delete()
    #return render(request, 'Lab3PPW/displayjadwal.html', {})
    return HttpResponseRedirect('/displayjadwal.html')
def schedule(request):
    if request.method == 'POST':
        form = Form(request.POST)
        if form.is_valid():
            hari          = request.POST['hari']
            tanggal       = request.POST['tanggal']
            jam           = request.POST['jam']
            nama_kegiatan = request.POST['nama_kegiatan']
            tempat        = request.POST['tempat']
            kategori      = request.POST['kategori']
            final_data = Schedule(
                hari     = hari,
                tanggal  = tanggal,
                jam      = jam,
                kegiatan = nama_kegiatan,
                tempat   = tempat,
                kategori = kategori
            )
            final_data.save()
            return HttpResponseRedirect('/displayjadwal.html')

    else:
        form = Form()
    return render(request,'Lab3PPW/forms.html', {'form': form})

def home(request):
    return render(request, 'Lab3PPW/home.html', {})

def portfolio(request):
    return render(request, 'Lab3PPW/portfolio.html', {})

def gallery(request):
    return render(request, 'Lab3PPW/gallery.html', {})

def aboutme(request):
    return render(request, 'Lab3PPW/aboutme.html', {})

def contact(request):
    return render(request, 'Lab3PPW/contact.html', {})

def showcase(request):
    return render(request, 'Lab3PPW/showcase.html', {})

def guestbook(request):
    return render(request, 'Lab3PPW/guestbook.html', {})

def displayjadwal(request):
    return render(request, 'Lab3PPW/displayjadwal.html', {})
