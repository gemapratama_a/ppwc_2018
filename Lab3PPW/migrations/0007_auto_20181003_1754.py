# Generated by Django 2.1.1 on 2018-10-03 10:54

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Lab3PPW', '0006_auto_20181003_1751'),
    ]

    operations = [
        migrations.AddField(
            model_name='schedule',
            name='hari',
            field=models.CharField(default='1', max_length=200),
        ),
        migrations.AddField(
            model_name='schedule',
            name='jam',
            field=models.TimeField(default=datetime.time(0, 0)),
        ),
        migrations.AddField(
            model_name='schedule',
            name='tanggal',
            field=models.DateField(default=datetime.date.today),
        ),
    ]
