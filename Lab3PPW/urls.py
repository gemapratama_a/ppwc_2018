from django.urls import path
from . import views
from .views import index, message_post, message_table

urlpatterns = [
    path('', views.index, name='index')
    url(r'^result_table', message_table, name='result_table')
    ]
