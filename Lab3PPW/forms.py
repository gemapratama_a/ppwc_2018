from django import forms
from Lab3PPW.models import Schedule
# https://simpleisbetterthancomplex.com/article/2017/08/19/how-to-render-django-form-manually.html
# https://www.youtube.com/watch?v=qwE9TFNub84

class Form(forms.ModelForm):
        PILIHAN_HARI = (
            ('Senin', 'Senin'),
            ('Selasa', 'Selasa'),
            ('Rabu', 'Rabu'),
            ('Kamis', 'Kamis'),
            ('Jumat', 'Jumat'),
            ('Sabtu', 'Sabtu'),
            ('Minggu', 'Minggu')
        )
        PILIHAN_KATEGORI = (
            ('Hiburan', 'Hiburan'),
            ('Akademis', 'Akademis'),
            ('Lain-lain', 'Lain-lain')
        )
        hari          = forms.ChoiceField(choices = PILIHAN_HARI, initial = '1')
        tanggal       = forms.DateField(widget = forms.DateInput(attrs = {'type' : 'date'}))
        jam           = forms.TimeField(widget = forms.TimeInput(attrs = {'type' : 'time'}))
        nama_kegiatan = forms.CharField(max_length = 200)
        tempat        = forms.CharField(max_length = 200)
        kategori      = forms.ChoiceField(choices = PILIHAN_KATEGORI)

        class Meta:
            model = Schedule
            fields = ('hari', 'tanggal', 'jam', 'nama_kegiatan', 'tempat', 'kategori')
